package ProblemaJoel;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;

import org.junit.jupiter.api.Test;

class PruebasElecciones {

	@Test
	public void testCalcularTotalEjercicios01() {

		LinkedHashMap<String, Integer> diccionario1 = new LinkedHashMap<>();
		String personas = "Ivan/2 Mael/8 Jordi/4";
		int nGente = 3;
		assertEquals("14 Mael nadie", Joel.calcularTotalEjercicios(diccionario1, personas, nGente));
	}

	@Test
	public void testCalcularTotalEjercicios02() {

		LinkedHashMap<String, Integer> diccionario2 = new LinkedHashMap<>();
		String personas = "Ivan/5 Mael/3 Jordi/2";
		int nGente = 3;
		assertEquals("10 Ivan nadie", Joel.calcularTotalEjercicios(diccionario2, personas,nGente));

	}

	@Test
	public void testCalcularTotalEjercicios03() {

		LinkedHashMap<String, Integer> diccionario3 = new LinkedHashMap<>();
		String personas = "Ivan/5";
		int nGente = 1;
		assertEquals("5 Ivan nadie", Joel.calcularTotalEjercicios(diccionario3, personas,nGente));

	}

	@Test
	public void testCalcularTotalEjercicios04() {

		LinkedHashMap<String, Integer> diccionario4 = new LinkedHashMap<>();
//		diccionario4.put("Ivan", 0);
//		diccionario4.put("Mael", 0);
//		diccionario4.put("Jordi", 0);
		String personas = "Ivan/0 Mael/0 Jordi/0";
		int nGente = 3;
		assertEquals("0 nadie nadie", Joel.calcularTotalEjercicios(diccionario4,personas,nGente));

	}

	@Test
	public void testCalcularTotalEjercicios05() {

		LinkedHashMap<String, Integer> diccionario5 = new LinkedHashMap<>();
//		diccionario5.put("Ivan", -5);
//		diccionario5.put("Mael", -3);
//		diccionario5.put("Jordi", -2);
		String personas = "Ivan/-5 Mael/-3 Jordi/-2";
		int nGente = 3;
		assertEquals("-10 nadie Ivan", Joel.calcularTotalEjercicios(diccionario5,personas,nGente));

	}

	@Test
	public void testCalcularTotalEjercicios06() {

		LinkedHashMap<String, Integer> diccionario6 = new LinkedHashMap<>();
//		diccionario6.put("Ivan", 2);
//		diccionario6.put("Mael", -4);
//		diccionario6.put("Jordi", 5);
		String personas = "Ivan/2 Mael/-4 Jordi/5";
		int nGente = 3;
		assertEquals("3 Jordi Mael", Joel.calcularTotalEjercicios(diccionario6,personas,nGente));

	}

	@Test
	public void testCalcularTotalEjercicios07() {

		LinkedHashMap<String, Integer> diccionario7 = new LinkedHashMap<>();
//		diccionario7.put("Ivan", 2);
//		diccionario7.put("Mael", -10);
//		diccionario7.put("Jordi", 4);
		String personas = "Ivan/2 Mael/-10 Jordi/4";
		int nGente = 3;
		assertEquals("-4 Jordi Mael", Joel.calcularTotalEjercicios(diccionario7,personas,nGente));

	}

	@Test
	public void testCalcularTotalEjercicios08() {

		LinkedHashMap<String, Integer> diccionario8 = new LinkedHashMap<>();
//		diccionario8.put("Ivan", 100);
//		diccionario8.put("Mael", 200);
//		diccionario8.put("Jordi", 300);
		String personas = "Ivan/100 Mael/200 Jordi/300";
		int nGente = 3;
		assertEquals("600 Jordi nadie", Joel.calcularTotalEjercicios(diccionario8,personas,nGente));

	}

	@Test
	public void testCalcularTotalEjercicios09() {

		LinkedHashMap<String, Integer> diccionario9 = new LinkedHashMap<>();
//		diccionario9.put("Ivan", 1);
//		diccionario9.put("Mael", 2);
//		diccionario9.put("Jordi", 3);
		String personas = "Ivan/1 Mael/2 Jordi/3";
		int nGente = 3;
		assertEquals("6 Jordi nadie", Joel.calcularTotalEjercicios(diccionario9,personas,nGente));

	}

	@Test
	public void testCalcularTotalEjercicios10() {

		LinkedHashMap<String, Integer> diccionario10 = new LinkedHashMap<>();
//		diccionario10.put("Ivan", 1);
//		diccionario10.put("Mael", 2);
//		diccionario10.put("Jordi", 10);
		String personas = "Ivan/1 Mael/2 Jordi/10";
		int nGente = 3;
		assertEquals("13 Jordi nadie", Joel.calcularTotalEjercicios(diccionario10,personas,nGente));

	}

	@Test
	public void testCalcularTotalEjercicios11() {

		LinkedHashMap<String, Integer> diccionario11 = new LinkedHashMap<>();
//		diccionario11.put("Ivan", 1);
//		diccionario11.put("Mael", -2);
//		diccionario11.put("Jordi", 3);
		String personas = "Ivan/1 Mael/-2";
		int nGente = 2;
		assertEquals("-1 Ivan Mael", Joel.calcularTotalEjercicios(diccionario11,personas,nGente));

	}

	@Test
	public void testCalcularTotalEjercicios12() {

		LinkedHashMap<String, Integer> diccionario12 = new LinkedHashMap<>();
//		diccionario12.put("Ivan", -1);
//		diccionario12.put("Mael", 2);
//		diccionario12.put("Jordi", -3);
		String personas = "Ivan/-1 Mael/2 Jordi/-3";
		int nGente = 3;
		assertEquals("-2 Mael Jordi", Joel.calcularTotalEjercicios(diccionario12,personas,nGente));

	}

	@Test
	public void testCalcularTotalEjercicios13() {

		LinkedHashMap<String, Integer> diccionario13 = new LinkedHashMap<>();
//		diccionario13.put("Ivan", 10000);
		String personas = "Ivan/10000";
		int nGente = 1;
		assertEquals("10000 Ivan nadie", Joel.calcularTotalEjercicios(diccionario13,personas,nGente));

	}

	@Test
	public void testCalcularTotalEjercicios14() {

		LinkedHashMap<String, Integer> diccionario14 = new LinkedHashMap<>();
//		diccionario14.put("Ivan", 100);
//		diccionario14.put("Mael", -50);
//		diccionario14.put("Jordi", 1000);
		String personas = "Ivan/100 Mael/-50 Jordi/1000";
		int nGente = 3;
		assertEquals("1050 Jordi Mael", Joel.calcularTotalEjercicios(diccionario14,personas,nGente));

	}

	@Test
	public void testCalcularTotalEjercicios15() {
		LinkedHashMap<String, Integer> diccionario15 = new LinkedHashMap<>();
//		diccionario15.put("Ivan", 1000);
//		diccionario15.put("Mael", -500);
//		diccionario15.put("Jordi", 10000);
		String personas = "Ivan/1000 Mael/-500 Jordi/10000";
		int nGente = 3;
		assertEquals("10500 Jordi Mael", Joel.calcularTotalEjercicios(diccionario15,personas,nGente));

	}

	@Test
	public void testCalcularTotalEjercicios16() {
		LinkedHashMap<String, Integer> diccionario16 = new LinkedHashMap<>();
//		diccionario16.put("Ivan", 100);
//		diccionario16.put("Mael", -200);
//		diccionario16.put("Jordi", 300);
		String personas = "Ivan/100 Mael/-200 Jordi/300";
		int nGente = 3;
		assertEquals("200 Jordi Mael", Joel.calcularTotalEjercicios(diccionario16,personas,nGente));

	}

	@Test
	public void testCalcularTotalEjercicios17() {

		LinkedHashMap<String, Integer> diccionario17 = new LinkedHashMap<>();
//		diccionario17.put("Ivan", 50);
//		diccionario17.put("Mael", 30);
//		diccionario17.put("Jordi", -70);
		String personas = "Ivan/50 Mael/30 Jordi/-70";
		int nGente = 3;
		assertEquals("10 Ivan Jordi", Joel.calcularTotalEjercicios(diccionario17,personas,nGente));

	}

	@Test
	public void testCalcularTotalEjercicios18() {
		LinkedHashMap<String, Integer> diccionario18 = new LinkedHashMap<>();
//		diccionario18.put("Ivan", -10);
//		diccionario18.put("Mael", 20);
//		diccionario18.put("Jordi", 30);
		String personas = "Ivan/-10 Mael/20 Jordi/30";
		int nGente = 3;
		assertEquals("40 Jordi Ivan", Joel.calcularTotalEjercicios(diccionario18,personas,nGente));

	}

	@Test
	public void testCalcularTotalEjercicios19() {

		LinkedHashMap<String, Integer> diccionario19 = new LinkedHashMap<>();
//		diccionario19.put("Ivan", -10);
//		diccionario19.put("Mael", -5);
//		diccionario19.put("Jordi", 15);
		String personas = "Ivan/-10 Mael/-5 Jordi/15";
		int nGente = 3;
		assertEquals("0 Jordi Ivan", Joel.calcularTotalEjercicios(diccionario19,personas,nGente));

	}

	@Test
	public void testCalcularTotalEjercicios20() {
		LinkedHashMap<String, Integer> diccionario20 = new LinkedHashMap<>();
//		diccionario20.put("Ivan", 1);
//		diccionario20.put("Mael", 2);
//		diccionario20.put("Jordi", -3);
		String personas = "Ivan/1 Mael/2 Jordi/-3";
		int nGente = 3;
		assertEquals("0 Mael Jordi", Joel.calcularTotalEjercicios(diccionario20,personas,nGente));

	}

}
