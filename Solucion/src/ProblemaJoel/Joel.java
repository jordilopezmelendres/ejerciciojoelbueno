/**
 * Nombre del package Joel
 */
package ProblemaJoel;

import java.util.LinkedHashMap;

/**
 * Clase que contiene un método para calcular el total de ejercicios realizados por un grupo y determinar quién ha hecho más ejercicios.
 * También identifica a la persona que ha hecho trampas (si la hay).
 */
public class Joel {
	
	/**
     * Método principal para ejecutar el programa de ejemplo.
     *
     * @param args Argumentos de la línea de comandos (no utilizado en este programa).
     */
    public static void main(String[] args) {
        LinkedHashMap<String, Integer> diccionario = new LinkedHashMap<>();
//        diccionario.put("Ivan", 2);
//        diccionario.put("Mael", 8);
//        diccionario.put("Jordi", 4);
        
        String personas = "Ivan/2 Mael/-8 Jordi/-4";

        calcularTotalEjercicios(diccionario,personas,3);
    }

    /**
     * Calcula el total de ejercicios realizados por el grupo, la persona que ha hecho más ejercicios y la persona que ha hecho trampas (si la hay).
     *
     * @param diccionario Un diccionario que mapea nombres de personas a la cantidad de ejercicios que han realizado.
     * @return Una cadena de texto que contiene el total de ejercicios, la persona que ha hecho más ejercicios y la persona que ha hecho trampas.
     */
    public static String calcularTotalEjercicios(LinkedHashMap<String, Integer> diccionario, String gente, int nGente) {
        int totalEjercicios = 0;
        String maxEjercicios = "nadie";
        int maxCantidad = 0;
        int maxTramposoCantidad = 0;
        String maxTramposo = "nadie";
        
        for (int j = 0; j < nGente; j++) {

			String anime[] = gente.split(" ");
			
			String anime2[] = anime[j].split("/");

			int valor = Integer.parseInt(anime2[1]);

			String clau = anime2[0];

			diccionario.put(clau, valor);

		}

        for (String s : diccionario.keySet()) {
            totalEjercicios += diccionario.get(s);

            if (diccionario.get(s) > maxCantidad) {
                maxCantidad = diccionario.get(s);
                maxEjercicios = s;
            } else if (diccionario.get(s) < maxTramposoCantidad && diccionario.get(s) < 0) {
                maxTramposoCantidad = diccionario.get(s);
                maxTramposo = s;
            }
        }

        System.out.println(totalEjercicios +" "+ maxEjercicios +" "+ maxTramposo);
        return totalEjercicios + " " + maxEjercicios + " " + maxTramposo;
    }

    
}